package mytests.DBImitation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Pavel Osipenko on 26.05.2016.
 *
 */
public class FakeDB {

    public static void main(String[] args) {
        List<Item> db = new ArrayList<>();

        // Fills the database
        for (int i = 0; i < 3; i++) {
            db.add(new Item("KEK", new Date(), 3.254, 5, 664375377));
        }

        // Write data from DB to console
        for (Item it : db) {
            System.out.printf("%s, %tD, %s, %s, %s\n", it.one, it.two, it.three, it.four, it.five);
        }
    }
}
