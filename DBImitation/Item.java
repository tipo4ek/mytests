package mytests.DBImitation;

import java.util.Date;

/**
 * Created by PavelOsipenko on 26.05.2016.
 */
public class Item {
    public String one;
    public Date two;
    public double three;
    public int four;
    public long five;

    public Item(String one, Date two, double three, int four, long five) {
        this.one = one;
        this.two = two;
        this.three = three;
        this.four = four;
        this.five = five;
    }
}
