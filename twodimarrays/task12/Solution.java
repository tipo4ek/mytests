package mytests.twodimarrays.task12;

/**
 * Created by mOPs on 11.04.2016.
 */

/*
Вставить второй столбец после первого столбца, в котором все элементы положительны.
Если такого столбца нет, то вывести сообщение об этом.
 */
public class Solution {

    public static void main(String[] args) {
        int[][] a = {
                {-1, 2, 3, 4, 5},
                {1, -2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, -2, 3, -4, 5},
                {1, 2, 3, 4, 5}

        };
        int[][] b = insertSecondColumn(a);
        showArray(b);
    }

    private static int[][] insertSecondColumn(int[][] a) {
        int[][] b = new int[a.length][a[0].length + 1];
        for (int i = 0; i < a[i].length; i++) {
            int indexOfPositiveColumn = -1;
            for (int j = 0; j < a.length; j++) {
                if (a[j][i] >= 0) {
                    indexOfPositiveColumn = i;
                } else {
                    indexOfPositiveColumn = -1;
                    break;
                }
            }
            if (indexOfPositiveColumn != -1) {
                return getNewArray(a, indexOfPositiveColumn);
            }
        }
        return a;
    }

    private static int[][] getNewArray(int[][] a, int column) {
        int[][] b = new int[a.length][a[0].length + 1];
        int jB;
        for (int i = 0; i < b.length; i++) {
            int jA = 0;
            for (jB = 0; jB < b[i].length; jB++) {
                if (jB == column + 1) {
                    b[i][jB] = a[i][1];
                    jA--;
                } else {
                    b[i][jB] = a[i][jA];
                }
                jA++;
            }
        }
        return b;
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}
