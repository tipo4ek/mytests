package mytests.twodimarrays.task11;

/**
 * Created by mOPs on 11.04.2016.
 */

/*
Переверните в масиве каждую третью строку
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {0, 0, 0, 1, 1, 1, 1},
                {0, 0, 0, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6, 7},
                {0, 0, 0, 1, 1, 1, 1},
                {0, 0, 0, 1, 1, 1, 1},
                {7, 8, 9, 0, 10, 11, 12},
                {0, 0, 0, 1, 1, 1, 1}
        };
        reversEachOfThirdRow(a);
        showArray(a);
    }

    private static void reversEachOfThirdRow(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            if ((i + 1) % 3 == 0) {
                reverseRowElements(a[i]);
            }
        }
    }

    private static void reverseRowElements(int[] row) {
        for (int i = 0; i < row.length / 2; i++) {
            int temp = row[i];
            row[i] = row[row.length - i - 1];
            row[row.length - i - 1] = temp;
        }
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}
