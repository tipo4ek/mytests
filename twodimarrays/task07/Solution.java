package mytests.twodimarrays.task07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mOPs on 09.04.16.
 */

/*
Дана действительная матрица размером N*M. Определить имеются ли в ней столбцы со знакочередующимися элементами.
 */
public class Solution {
    public static void main(String[] args) {

        double[][] a = {
                {10, -12, 13, -3},
                {-25, 51, -1, 3},
                {33, -88, 0, -3},
                {22, -11, 5, 3}
        };

        List<Boolean> list = isHaveSignAlternationElements(a);
        if (list.contains((boolean) true)) {
            System.out.println("В массиве имеются столбцы со знакочередующимися элементами: ");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i)) System.out.println("стоблец " + i);
            }
        } else {
            System.out.println("В массиве нет столбцов со знакочередующимися элементами!");
        }
    }

    private static List<Boolean> isHaveSignAlternationElements(double[][] a) {
        Boolean[] arr = new Boolean[a[0].length];
        Arrays.fill(arr, true);

        for (int j = 0; j < a[0].length; j++) {
            int previousSign = sign(a[0][j]);
            for (int i = 1; i < a.length; i++) {
                if (previousSign == sign(a[i][j])) {
                    arr[j] = false;
                    break;
                }
                previousSign = sign(a[i][j]);
            }
        }
        return new ArrayList<>(Arrays.asList(arr));
    }

    private static int sign(double x) {
        return x < 0 ? -1 : 1;
    }
}