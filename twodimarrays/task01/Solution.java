package mytests.twodimarrays.task01;

/**
 * Created by mOPs on 09.04.16.
 */

/*
Дана целочисленная квадратная матрица.
Определить сумму элементов в тех столбцх, которые не содержат отрицательных элементов.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {1, 0, 0},
                {0, 2, 0},
                {0, 0, -3},
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        };
        sumElementsInColumn(a);
    }

    private static void sumElementsInColumn(int[][] a) {
        int sum;
        boolean isResetSum;

        for (int i = 0; i < a[0].length; i++) {
            sum = 0;
            isResetSum = false;
            for (int j = 0; j < a.length; j++) {
                if (a[j][i] < 0) {
                  isResetSum = true;
                }
                sum += a[j][i];
            }
            if (!isResetSum) System.out.printf("Sum of column %s = %s\n", i, sum);
        }
    }
}