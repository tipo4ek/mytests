package mytests.twodimarrays.task03;

/**
 * Created by mOPs on 09.04.16.
 */

/*
Дана матрица. Втавить первую строку перед строкой, в которой находится первый минимальный элемент.
 */
public class Solution {
	public static void main(String[] args) {
	    int[][] a = {
            {0, 0, 0},
            {1, 1, 1},
            {2, 2, -3},
            {3, 3, -3},
            {4, 4, 4}
        };

        try {
            showArray(addRow(a));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //Create new array with a[0] row 
    //in position before row with minimal negative element
    private static int[][] addRow(int[][] a) throws Exception {
        int indexOfMinRow = findRowIncludeMinElement(a);
        if (indexOfMinRow < 0) {
            throw new Exception("Negative element not found in array");
        }
        int[][] resultArray = new int[a.length + 1][a.length];

        int j = 0;
        for (int i = 0; i < resultArray.length; i ++) {
            if (i == indexOfMinRow || indexOfMinRow == 0) {
                indexOfMinRow = -1;
                resultArray[i] = a[0];
                i++;  
            }
            resultArray[i] = a[j];
            j++;
        }
        return resultArray;
    }
    
    //Find row with minimal negative element
    private static int findRowIncludeMinElement(int[][] a) {
        int rowWithMinElement = -1;
        int minElement = a[0][0];
        for (int i =0; i < a.length; i++) {   
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < minElement) {
                    minElement = a[i][j];
                    rowWithMinElement = i;
                }
            }
        }
        return rowWithMinElement;
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}
