package mytests.twodimarrays.task09;

/**
 * Created by PavelOsipenko on 11.04.2016.
 */

/*
Проверить является массив логическим квадратом, то есть суммы по всем горизонталям, вертикалям и двум диагоналям должны быть равны.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {67, 1, 43},
                {13, 37, 61},
                {31, 73, 7}
        };
        System.out.println(isLogicalSquare(a));
    }

    private static boolean isLogicalSquare(int[][] a) {
        if (a.length != a[0].length) return false;

        int sum = 0;
        // Находим сумму первой горизонтали
        for (int i = 0; i < a.length; i++) {
            sum += a[0][i];
        }

        //Проверяем остальные горизонтали
        for (int i = 1; i < a.length; i++) {
            int sumHorisont = 0;
            for (int j = 0; j < a[i].length; j++) {
                sumHorisont += a[i][j];
            }
            if (sum != sumHorisont) return false;
        }

        //Проверка вертикалей
        for (int i = 0; i < a[0].length; i++) {
            int sumVertical = 0;
            for (int j = 0; j < a.length; j++) {
                sumVertical += a[j][i];
            }
            if (sum != sumVertical) return false;
        }

        //Проверяем диагонали
        int sumFirstDig = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (i == j) {
                    sumFirstDig += a[i][j];
                }
            }
        }
        if (sum != sumFirstDig) return false;

        int sumSecondGiag = 0;
        int iter1 = a.length-1;
        int iter2 = 0;
        for (int i = 0; i < a.length; i++) {
            sumSecondGiag += a[iter1][iter2];
            iter1--;
            iter2++;
        }
        return sum == sumSecondGiag;
    }
}

