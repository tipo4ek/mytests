package mytests.twodimarrays.task08;

/**
 * Created by mOPs on 11.04.2016.
 */

/*
Определить, действительно ли все элементы N-ой строки матрицы A (n,m) равны элементам первой строки.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {1, 0, 0},
                {0, 2, 0},
                {1, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        };

        for (int i = 0; i < a.length; i++) {
            System.out.println("Строка " + i + " = строке 0 ? " + isEqualFirstRow(a, i));
        }
    }

    private static boolean isEqualFirstRow(int[][] a, int currentRor) {
        int[] firstRow = a[0];
        for (int i = 0; i < a[currentRor].length; i++) {
            if (a[currentRor][i] != firstRow[i]) return false;
        }
        return true;
    }
}
