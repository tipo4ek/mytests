package mytests.twodimarrays.task06;

/**
 * Created by mOPs on 09.04.16.
 */

/*
В двумерном массиве размерностью n x m поменять местами минимальный и максимальны элементы каждой строки.
Вывести преобразованный массив.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {0, 1, 2},
                {1, 0, 2},
                {2, 0, 1},
                {2, 1, 0}
        };
        replaceMinMaxElements(a);
        showArray(a);
    }

    private static void replaceMinMaxElements(int[][] a) {
        int min;
        int max;
        int indexOfMin;
        int indexOfMax;

        for (int i = 0; i < a.length; i++) {
            min = a[i][0]; indexOfMin = 0;
            max = a[i][1]; indexOfMax = 1;
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < min) {
                    min = a[i][j];
                    indexOfMin = j;
                }
                if (a[i][j] > max) {
                    max = a[i][j];
                    indexOfMax = j;
                }
            }
            int temp = a[i][indexOfMax];
            a[i][indexOfMax] = a[i][indexOfMin];
            a[i][indexOfMin] = temp;
        }
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}

