package mytests.twodimarrays.task05;

/**
 * Created by mOPs on 09.04.16.
 */

/*
В двумерном массиве размерностью n x m определить минимальный по величине элемент и вычесть его их элементов
той строки, и того столбца, на пересечении которого он стоит.
Вывести преобразованный массив.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {8, 8, 8, 8, 8},
                {8, 8, 8, 8, 8},
                {8, 1, 8, 8, 8},

        };

        reformedArray(a);
        showArray(a);
    }

    private static void reformedArray(int[][] a) {
        int[] minElementIndex = findMinElementIndex(a); // индексы минимального элемента
        int rowIndex = minElementIndex[0];
        int columnIndex = minElementIndex[1];
        int minElement = a[rowIndex][columnIndex]; //минимальный элемент

        // Вычитаем минимальный элемент из элементов столбца
        for (int i = 0; i < a.length; i++) {
            if (i != rowIndex) {
                a[i][columnIndex] -= minElement;
            }
        }

        // Вычитаем минимальный элемент из элементов строки
        for (int j = 0; j < a[rowIndex].length; j++) {
            if (j != columnIndex) {
                a[rowIndex][j] -= minElement;
            }
        }
    }

    private static int[] findMinElementIndex(int[][] a) {
        int minElement = a[0][0];
        int[] indexOfMinElement = {0, 0};
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < minElement) {
                    minElement = a[i][j];
                    indexOfMinElement[0] = i;
                    indexOfMinElement[1] = j;
                }
            }
        }
        return indexOfMinElement;
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}


