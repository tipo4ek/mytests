package mytests.twodimarrays.task13;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mOPs on 12.04.16.
 */

/*
В массиве вставить после всех строк, в которых есть заданное число А, последнюю строку массива
 */
public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {0, 0, 0, 4, 0},
                {0, 4, 0, 0, 0},
                {8, 8, 8, 4, 8}
        };

        int specifiedNumber = 4;
        showArray(getNewArray(a, specifiedNumber));
    }

    private static int[][] getNewArray(int[][] a, int specifiedNumber) {
        Set<Integer> setOfRowWithSpecifiedNumbers = new HashSet<>();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] == specifiedNumber) {
                    setOfRowWithSpecifiedNumbers.add(i);
                }
            }
        }
        if (!setOfRowWithSpecifiedNumbers.isEmpty()) return insertLastRow(a, setOfRowWithSpecifiedNumbers);
        return a;
    }

    private static int[][] insertLastRow(int[][] a, Set<Integer> set) {
        int[][] b = new int[a.length + set.size()][a[0].length];
        int j = 0;
        for (int i = 0; i < b.length; i++) {
            b[i] = a[j];
            if (set.contains(j)) {
                i++;
                b[i] = a[a.length-1];
            }
            j++;
        }
        return b;
    }

    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}
