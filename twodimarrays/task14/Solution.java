package mytests.twodimarrays.task14;

import java.util.Random;

/**
 * Created by mOPs on 19.05.16.
 */
public class Solution {
    public static void main(String[] args) {
        int n  = 1000;
        int[][] a = new int[n][n];

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = new Random().nextInt(2);
            }
        }
        long stopTime = System.currentTimeMillis();
        System.out.printf("Fill array time = %d ms\n", stopTime - startTime);
        System.out.println(calculate(a));
    }

    public static int calculate(int[][] matrix) {
        long startTime = System.currentTimeMillis();
        int sum0 = 0;
        int sum1 = 0;

        for (int i = 0; i < matrix.length; i++) {
            sum0 += matrix[i][i];
            sum1 += matrix[matrix.length - 1 - i][i];
        }
        long stopTime = System.currentTimeMillis();
        System.out.printf("Calculate time = %d ms\n", stopTime - startTime);
        return sum0 * sum1;
    }
}
