package mytests.twodimarrays.task04;

/**
 * Created by mOPs on 09.04.16.
 */

/*
Вывести на экран номер и значения элементов строки из двумерного массива,
размерностью n x m, с наибольшим количеством положительных элементов
*/

public class Solution {
    public static void main(String[] args) {
        int[][] a = {
                {0, 0, 0},
                {-1, -1, -1},
                {2, -2, 2},
                {3, 3, 3}
        };
        showRow(a);
    }

    private static int findRowWithMaxPosElem(int[][] a) {
        int row = -1;
        int maxPosElements = 0;
        int counter;
        for (int i = 0; i < a.length; i++) {
            counter = 0;
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] > 0) counter++;
            }
            if (counter > maxPosElements) {
                maxPosElements = counter;
                row = i;
            }
        }
        return row;
    }

    private static void showRow(int[][] a) {
        int row = findRowWithMaxPosElem(a);
        System.out.println("Row " + row);
        for (int i = 0; i < a[row].length; i++) {
            System.out.print(a[row][i]);
        }
    }
}
