package mytests.twodimarrays.task10;

/**
 * Created by mOPs on 11.04.2016.
 */

/*
В массиве пследний отрицательный элемент каждого столбца заменить нулем
 */
public class Solution {

    public static void main(String[] args) {
        int[][] a = {
                {1, -1, 1, 1, -1, 1},
                {1, 1, 1, 1, 1, 1},
                {1, -1, 1, 1, -1, 1},
                {1, 1, 1, 1, 1, 1},

        };

        replaceLastNegativeElementByZero(a);
        showArray(a);
    }

    private static void replaceLastNegativeElementByZero(int[][] a) {
        int[] indexOfLastNegativeElement = {-1, -1};
        for (int i = 0; i < a[0].length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (a[j][i] < 0) {
                    indexOfLastNegativeElement[0] = j;
                    indexOfLastNegativeElement[1] = i;
                }
            }
            if ((indexOfLastNegativeElement[0] != -1) || (indexOfLastNegativeElement[1] != -1)) {
                a[indexOfLastNegativeElement[0]][indexOfLastNegativeElement[1]] = 0;
            }
        }
    }
    private static void showArray(int[][] a) {
        for (int[] anA : a) {
            for (int anAnA : anA) {
                System.out.print(anAnA + "\t");
            }
            System.out.println("");
        }
    }
}
