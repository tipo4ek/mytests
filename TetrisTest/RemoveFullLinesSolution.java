package mytests.TetrisTest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mops on 05.05.16.
 */
public class RemoveFullLinesSolution {
    //ширина и высота
    private static int width;
    private static int height;

    //матрица поля: 1 - клетка занята, 0 - свободна
    // matrix[ height ][ width ]
    private static int[][] matrix = {
            {0, 1, 1, 1},
            {1, 0, 1, 1},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
            {1, 1, 1, 1},

    };

    public static void main(String[] args) {
        width = matrix[0].length;
        height = matrix.length;

        System.out.println("before:");
        showMatrix(matrix);

        removeFullLines();
        System.out.println("after:");
        showMatrix(matrix);
    }

    public static void showMatrix(int[][] matrix) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(" " + matrix[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void removeFullLines() {
        //Например так:
        //Создаем список для хранения линий
        List<int[]> list = new ArrayList<>();

        //Копируем все непустые линии в список.
        for (int i = 0; i < height; i++) {
            boolean lineNotFull = false;
            for (int j = 0; j < width; j++) {
                if (matrix[i][j] == 0) {
                    lineNotFull = true;
                    break;
                }
            }
            if (lineNotFull) {
                list.add(matrix[i]);
            }
        }

        //Добавляем недостающие строки в начало списка.
        int addedLineQty = height - list.size();
        for (int i = 0; i < addedLineQty; i++) {
            list.add(0, new int[width]);
        }

        //Преобразуем список обратно в матрицу
        list.toArray(matrix);
    }
}
