package mytests.formaDataTest;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by mOPs on 16.10.16.
 */
public class Test {
    public static void main(String[] args) {

        Date d1 = new Date(2016, 01, 01);
        Date d2 = new Date(2016, 01, 01);

        System.out.println(d1.compareTo(d2));
    }

    public static Date getRandomDate(int startYear) {
        int year = startYear + (int) (Math.random() * 30);
        int month = (int) (Math.random() * 12);
        int day = (int) (Math.random() * 28);
        GregorianCalendar calendar = new GregorianCalendar(year, month, day);
        return calendar.getTime();
    }
}
