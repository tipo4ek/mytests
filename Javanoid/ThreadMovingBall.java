package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.math.*;

public class ThreadMovingBall extends Thread
{
	static JFrame frame;
	static GamePanel gamePanel;
	static GameBar gameBar;
	static Brick[][] bricks;
	private double speedBallX = 1;
	private double speedBallY = 1;
	private int angle = 120;
	private Random rnd;
	private BricksMap bricksMap;
	private boolean isMove = false;
	private int sleepTime = 2;
		
	public ThreadMovingBall(JFrame f, GamePanel gp, GameBar gb, Brick[][] brc, BricksMap bm)
	{
		frame = f;
		gamePanel = gp;
		gameBar = gb;
		bricks = brc;
		rnd = new Random();
		bricksMap = bm;
	}
	
	public void run()
	{
		this.movingBall();
		System.out.println(this.isInterrupted());
		
	}
	
	public void reloadMap()
	{
		bricks = bricksMap.getMap();
	}
	
	public boolean isMove()
	{
		return isMove;
	}
	
	public void setMove(boolean m)
	{
		isMove = m;
	}
	
	public void movingBall()
	{
		try
		{
		while(!this.isInterrupted())
		{
			speedBallX = Math.cos(angle * Math.PI / 180);
			speedBallY = Math.sin(angle * Math.PI / 180);
			
			if(isMove)
			{
				checkCollision(	gamePanel.getBallXPos(), gamePanel.getBallYPos(), gamePanel.getBallDia(),
							gamePanel.getCartXPos(), gamePanel.getCartYPos(), gamePanel.getCartXSize(), gamePanel.getCartYSize());
			
				gamePanel.moveBall(speedBallX, speedBallY);
			}
					
			Thread.sleep(sleepTime);
				
			
			
		}
		}catch (Exception ex){System.out.println("exception in movingBall()");}
		
		
	}
	
	public void checkCollision(double ballX, double ballY, int ballDia, int cartX, int cartY, int cartXSize, int cartYSize)
	{
				
					//-----------------------------------check the brick---------------------------------------
			for(int y = 0; y < 7; y++)
			{
				for(int x = 0; x < 16; x++)
				{
					double ballXCenter = ballX + (ballDia / 2);
					double ballYCenter = ballY + (ballDia / 2);
					
					int cartXCenter = cartX + (cartXSize / 2);
					int cartYCenter = cartY + (cartYSize / 2);
					
					//****************************************************
					int brickYDown =	 bricks[y][x].getBrickYDown();
					int brickYUP =		 bricks[y][x].getBrickYPos();
					int brickXLeft =	 bricks[y][x].getBrickXPos();
					int brickXRight =	 bricks[y][x].getBrickXRight();
					int brickHealth =	 bricks[y][x].getHealth();
					//****************************************************
					
					//brick bottom
										
					if(	 ballY < brickYDown && 
						 ballY + ballDia > brickYDown &&
						 
						 ballXCenter > brickXLeft && 
						 ballXCenter < brickXRight && 
						 brickHealth > 0 )
					{
						gameBar.addScore(bricks[y][x].getHealth());
						gameBar.repaint();
						
						bricks[y][x].downHealth();
						gamePanel.setBallYPos(brickYDown + ballDia + 1);
						angle = 360 - angle;
						
					}
					 //brick top
										
					if(	 ballY + ballDia > brickYUP && 
						 ballY + ballDia < brickYDown &&
						 
						 ballXCenter > brickXLeft && 
						 ballXCenter < brickXRight && 
						 brickHealth > 0 )
					{
						gameBar.addScore(bricks[y][x].getHealth());
						gameBar.repaint();
						
						bricks[y][x].downHealth();
						gamePanel.setBallYPos(brickYUP - ballDia - 1);
						angle = 360 - angle;
					}
					
					
					//right edge
					if( ballX < brickXRight &&
						ballX + ballDia > brickXRight &&
						
						ballYCenter < brickYDown &&
						ballYCenter > brickYUP &&
						brickHealth > 0 )
						{
							gameBar.addScore(bricks[y][x].getHealth());
							gameBar.repaint();
						
							bricks[y][x].downHealth();
							gamePanel.setBallXPos(brickXRight + 1);
							angle = 180 - angle;
						}
					
					//Left edge
					if( ballX + ballDia > brickXLeft &&
						ballX + ballDia < brickXRight &&
						
						ballYCenter < brickYDown &&
						ballYCenter > brickYUP &&
						brickHealth > 0 )
						{
							gameBar.addScore(bricks[y][x].getHealth());
							gameBar.repaint();
						
							bricks[y][x].downHealth();
							gamePanel.setBallXPos(brickXLeft - ballDia - 1);
							angle = 180 - angle;
							 if (angle < 0) 
							 {
								 angle += 360;
							 }
						}
					

				
				}
				
				
			}
			
			
			//-----------------------------------------------------------------------------------------

				if( ballX > gamePanel.getWidth() - ballDia ) 
				{
					//speedBallX = changeSign(speedBallX);
					gamePanel.setBallXPos(gamePanel.getWidth() - ballDia - 1);
					angle = 180 - angle;
					if (angle < 0) 
					{
						angle += 360;
					}
					
				}
				
				if( ballX < 0 ) 
				{
					//speedBallX = changeSign(speedBallX);
					gamePanel.setBallXPos(1.0);
					angle = 180 - angle;
					
				}
	//------------------------------------------fall to the cart------------------------------------------------------
					
									
					//int cartXCenter = cartX + (cartXSize / 2);
					//int cartYCenter = cartY + (cartYSize / 2);
					double ballXCenter = ballX + (ballDia / 2);
					double ballYCenter = ballY + (ballDia / 2);
					
					int cartXStep = cartXSize / 5;
					int cartXsectorL2 = cartX + cartXStep;
					int cartXsectorL1 = cartX + cartXStep + cartXStep;
					int cartXsectorR1 = cartX + cartXStep + cartXStep + cartXStep;
					int cartXsectorR2 = cartX + cartXStep + cartXStep + cartXStep + cartXStep;
					
					double bounce = 180 / cartXSize;
					
					//****************************************************
				if( ballY + ballDia > cartY
					&& ballXCenter > cartX 
					&& ballXCenter < cartX + cartXSize ) 
				{
					gamePanel.setBallYPos(cartY - ballDia - 1);
					angle = (int) (180 + ((ballXCenter - cartX) * bounce)) + 10;
					System.out.println(angle);

				}
				
	//------------------------------------------fall to the floor-----------------------------------------------------		
				if( ballY > cartY ) 
				{
					gamePanel.setDefaultPosition();
					isMove = false;

					gameBar.downLives(); 
					gameBar.repaint();
					
					
					


				}
	//------------------------------------------fall to the floor-----------------------------------------------------		
				if( ballY < 0 ) 
				{
					gamePanel.setBallYPos(1.0);
					angle = 360 - angle;
				}

	}
	


	
	public double changeSign(double i)
	{
		if(i > 0) 
		{
			return -i;
		}else if(i < 0)
		{
			return Math.abs(i);
		}else return 0;
 
	}
	
	public int changeAngle(int an)
	{
		
		if( an >= 360)
		{
			return 180 + (rnd.nextInt(20) - 10);
		}else if(an <= 0)
		{
			return 180 + (rnd.nextInt(20) - 10);
		}else
		{
			an = an + (an + (rnd.nextInt(20) - 10) ); // ���� + (���� +- 10 ��������)
		
			
			return an;
		}
	}
	
	
	
}