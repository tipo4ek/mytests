package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.geom.Ellipse2D;

public class GamePanel extends JPanel implements Runnable
{

	
	private double xBall = 305;
	private double yBall = 225;
	private int diaBall = 10;
	
	private int xCart = 300;
	private int yCart = 580;
	private int xSizeCart = 55;
	private int ySizeCart = 15;
	
	private Brick [][] bricks;
	private BricksMap bricksMap;
	static GameBar gameBar;
	
	public GamePanel(Brick[][] br, GameBar gb, BricksMap bm)
	{
		bricks = br;
		gameBar = gb;
		bricksMap = bm;
		
		gameBar.setGamePanel(this);
	}
	
	public void run()
	{
		while(true)
		{
			repaint();
			try
			{
				Thread.sleep(20);
			}catch (Exception ex){}
		}
	}
	
	public void paintComponent(Graphics g)
	{
		this.setSize(640, 660);
		
		Graphics2D g2d = (Graphics2D) g;
	//-----------------------BACKGROUND----------------------------
		GradientPaint grad = new GradientPaint(0,0, new Color(0,0,255), this.getWidth(), this.getHeight(), new Color(255, 255, 255));
		g2d.setPaint(grad);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	//---------------------------BRICKS----------------------------
		 int xB = 0;
		 int yB = 0;
		 int bh = 0;
		Color brickColor = new Color(255, 255, 255);
		
		 for(int y = 0; y < 7; y++)
		 {
			 for(int x = 0; x < 16; x++)
			 {
				bh = bricks[y][x].getHealth();
				if(bh > 0)
				{
					g2d.setColor(new Color(15 + (bh * 30), 0, 0));
					g2d.fillRect(bricks[y][x].getBrickXPos(), bricks[y][x].getBrickYPos(), bricks[y][x].getBrickXSize(), bricks[y][x].getBrickYSize());
					g2d.setColor(Color.BLACK);
					g2d.drawRect(bricks[y][x].getBrickXPos(), bricks[y][x].getBrickYPos(), bricks[y][x].getBrickXSize(), bricks[y][x].getBrickYSize());
				}
			 }

		 }

	
	//-----------------------------BALL----------------------------	
 
		g2d.setColor(new Color(0, 0, 0));
		g2d.fill(circle(xBall, yBall));
		
		g2d.setColor(new Color(0, 0, 0));
		g2d.fill(circleHole(xBall, yBall));
	//-----------------------------CART----------------------------	
		
		g2d.setColor(new Color(0, 0, 0));
		g2d.fillRoundRect(xCart, yCart, xSizeCart, ySizeCart, 8, 8);
		
		g2d.setColor(new Color(0, 0, 0));
		g2d.drawRoundRect(xCart, yCart, xSizeCart, ySizeCart, 8, 8);
		
		for(int i = 0; i < 5; i++)
		{
			g2d.setColor(new Color(25, 25, 25));
			int xh = xSizeCart / 5 * i;
			int yh = ySizeCart / 2;
			g2d.fillOval(xCart + xh + 3, yCart + yh - (ySizeCart / 10) - 1, xSizeCart / 15 + 1, xSizeCart / 15 + 1); 
		}
		

		
		for(int i = 0; i < gameBar.getLives(); i++)
		{
			g2d.setColor(new Color(0, 50 + (i * 50), 0));
			int xh = xSizeCart / 5 * i;
			int yh = ySizeCart / 2;
			g2d.fillOval(xCart + xh + 3, yCart + yh - (ySizeCart / 10) - 1, xSizeCart / 15 + 1, xSizeCart / 15 + 1); 
		}
		
	}
	
	private Shape circle(double x, double y) 
	{
        return new Ellipse2D.Double(x, y, diaBall, diaBall);
    }
	
	private Shape circleHole(double x, double y)
	{
        return new Ellipse2D.Double(x + 1, y + 1, diaBall-2, diaBall-2);
    }
	
	public void moveBall(double pX, double pY)
	{
		xBall += pX;
		yBall += pY;
				
	}
	
	public void setDefaultPosition()
	{
		xBall = 305;
		yBall = bricksMap.getYTotal() + 200;
		xCart = 300;
		
	}
	
		public void moveCart(int pX)
	{
		xCart += pX;
		
				
	}
	
	public void setCartXPos(int x)
	{
		xCart = x;
	}
	
	public void reloadMap(Brick[][] br11)
	{
		bricks = br11;
	}
	
	//=========================================================ANIMATION=====================================================
	

	
	//===================================================================GET_BALL==============================================================
	
	public double getBallXPos()
	{
		return xBall;
	}
	
	public double getBallYPos()
	{
		return yBall;
	}
	
	public int getBallDia()
	{
		return diaBall;
	}
	
	//====================================================================GET_CART=================================================================
	public int getCartXPos()
	{
		return xCart;
	}
	
	public int getCartYPos()
	{
		return yCart;
	}
	
	public int getCartXSize()
	{
		return xSizeCart;
	}
	
	public int getCartYSize()
	{
		return ySizeCart;
	}
	
	//----------------------------------------------------------------------SET_BALL---------------------------------------------------------------------
	
	public void setBallXPos(double x)
	{
		xBall = x;
	}
	
	public void setBallYPos(double y)
	{
		yBall = y;
	}
	
}
