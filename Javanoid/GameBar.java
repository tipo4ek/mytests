package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class GameBar extends JPanel
{
	private int lives = 5;
	private int score = 0;
	private int bestScore = 0;
	private int xLivePos = 5;
	private int yLivePos = 5;
	
	private BricksMap bricksMap;
	private ThreadMovingBall ball;
	private Solution frame;
	private GamePanel gamePanel;
	
	private JLabel scoreLabel;
	private JLabel scoreText;
	
	private JLabel scoreBest;
	private JLabel scoreBestText;
	
	public GameBar(BricksMap bm, Solution fr)
	{
		bricksMap = bm;
		frame = fr;
		
		try
		{
			FileInputStream fs = new FileInputStream("Score.bst");
			ObjectInputStream os = new ObjectInputStream(fs);
			bestScore = (int) os.readObject();
			os.close();
		}catch(Exception ex)
		{
			
		}
		Font font =		 new Font("Impact", Font.BOLD, 30);
		Font fontText =	 new Font("Impact", Font.BOLD, 15);
		
		scoreLabel =	 new JLabel(Integer.toString(score));
		scoreText =		 new JLabel("Score: ");
		scoreBest =		 new JLabel(Integer.toString(bestScore));
		scoreBestText =	 new JLabel("Best: ");
		
		scoreLabel.setFont(font);
		scoreText.setFont(fontText);
		scoreBest.setFont(font);
		scoreBestText.setFont(fontText);
		
		Color textColor = new Color(0, 0, 0);
		
		scoreLabel.setForeground(textColor);
		scoreText.setForeground(textColor);
		scoreBest.setForeground(textColor);
		scoreBestText.setForeground(textColor);
		
		this.add(scoreText);
		this.add(scoreLabel);
		this.add(scoreBestText);
		this.add(scoreBest);
		
	}
	
	public void isAliveCart()
	{
		ball = frame.getBall();
		
		if(lives < 1)
		{
			bricksMap.setDefaultMap();
			ball.setMove(false);
			ball.reloadMap();
			gamePanel.reloadMap(bricksMap.getMap());
			lives = 5;
			try
			{
				FileOutputStream fs = new FileOutputStream("Score.bst");
				
				ObjectOutputStream os = new ObjectOutputStream(fs);
				
				os.writeObject(getBestScore());
								
				os.close();
				
				repaint();
				
			}catch (Exception ex)
			{
				
			}
			
			bestScore = getBestScore();
			scoreBest.setText(Integer.toString(bestScore));
			score = 0;
			repaint();
			
			
			
		}
	}
	
	public void setGamePanel(GamePanel gp)
	{
		gamePanel = gp;
	}
	
	
	public void paintComponent(Graphics g) 
	{
		xLivePos = 5;
		yLivePos = 5;
		
		Graphics2D g2d = (Graphics2D) g;

		GradientPaint grad = new GradientPaint(0,this.getHeight(), new Color(0,0,255), this.getWidth(), 0, new Color(255, 255, 255));
		g2d.setPaint(grad);

		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		scoreLabel.setText(Integer.toString(score));
	}
	
	//=======================GET==================================================
	
	public int getLives()
	{
		return lives;
	}
	
	public void setBestScore()
	{
		
	}
	
	public int getBestScore()
	{
		return (bestScore > score) ? bestScore : score;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void addScore(int s)
	{
		score += s;
	}
	
	//======================SET====================================================
	
	public void downLives()
	{
		this.lives -= 1;
		isAliveCart();
	}
	
	public void setLives(int l)
	{
		lives = l;
		isAliveCart();
	}
		
		
}
