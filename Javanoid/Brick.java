package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class Brick
{
	private int xBrick = 0;
	private int yBrick = 0;
	
	private int xBrickSize = 40;
	private int yBrickSize = 20;
	
	private int brickHealth = 0;
	
	public Brick(int x, int y, int bh)
	{
		xBrick = x;
		yBrick = y;
		
		brickHealth = bh;
	}
	
	//==========================GET========================================
	
	public int getHealth()
	{
		return brickHealth;
	}
	
	public int getBrickXPos()
	{
		return xBrick;
	}
	
	public int getBrickYPos()
	{
		return yBrick;
	}
	
	public int getBrickXRight()
	{
		return xBrick + xBrickSize;
	}
	
	public int getBrickYDown() 
	{
		return yBrick + yBrickSize;
	}
	
	public int getBrickXSize()
	{
		return xBrickSize;
	}
	
	public int getBrickYSize()
	{
		return yBrickSize;
	}
	
	//====================================SET========================================
	
	public void downHealth()
	{
		brickHealth -= 2;
	}
	
	public void setXYBrick(int x, int y)
	{
		xBrick = x;
		yBrick = y;
	}
	
}
