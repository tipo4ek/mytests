package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class ThreadKeyLis extends Thread
{
	static JFrame frame;
	static GamePanel gamePanel;
	static GameBar gameBar;
	static ThreadMovingBall ball;

	private int speedOfCart = 12;
	private int scorebst = 0;
	private int timerEvent = 0;
	private BricksMap bricksMap;

	private boolean isBallStarted = false;
	
	public ThreadKeyLis(JFrame f, GamePanel gp, ThreadMovingBall b, GameBar gm, BricksMap bm)
	{
		frame = f;
		gamePanel = gp;
		ball = b;
		gameBar = gm;
		bricksMap = bm;

	}
	
	public void run()
	{
		while(!this.isInterrupted())
		{
			try{
				Thread.sleep(1000);
				if(ball.isMove())
				{
					timerEvent++;
				}
				System.out.println(timerEvent);
				if(timerEvent >= 20 && ball.isMove())
				{
					timerEvent = 0;
					bricksMap.doStep();
					if( (bricksMap.getYTotal() + 200) > (gamePanel.getCartYPos() - 75) )
					{
						gameBar.setLives(0);
						gamePanel.setDefaultPosition();
					}
					
				}
			}catch(Exception ex){}
			
		}
	}
	
	public class KeyLis implements KeyListener
	{
		public void keyPressed(KeyEvent e)
		{
			if(!ball.isMove())
			{
				ball.setMove(true);
				
			}

			if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				gamePanel.moveCart(speedOfCart);
				
			}else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				gamePanel.moveCart(-speedOfCart);
				
			}
						
			
		}
		
		public void keyReleased(KeyEvent e)
		{
		
			
			
		}
		
		public void keyTyped(KeyEvent e)
		{
			
			
		}
	}
	
	public class MouseLis implements MouseMotionListener
	{
		public void mouseDragged(MouseEvent e)
		{
			
		}

		public void mouseMoved(MouseEvent e)
		{

			gamePanel.setCartXPos(e.getX() - (gamePanel.getCartXSize() / 2) );
			
			
		}


	}
	
	public class MouseKeyLis implements MouseListener
	{
		public void mouseClicked(MouseEvent e)
		{
			
		}

		public void mouseEntered(MouseEvent e)
		{
			
		}

		public void mouseExited(MouseEvent e)
		{
			
		}

		public void mousePressed(MouseEvent e)
		{
			if(e.getButton() == MouseEvent.BUTTON1)
			{
				if(!ball.isMove())
			{
				ball.setMove(true);
				
			}
				
			}
			
		}

		public void mouseReleased(MouseEvent e)
		{
			
		}

	}
	
	public class FrameLis implements WindowListener
	{
		public void	windowActivated(WindowEvent e){}

		public void	windowClosed(WindowEvent e){}

		public void	windowClosing(WindowEvent e)
		{
		
			
			try
			{
				FileOutputStream fs = new FileOutputStream("Score.bst");
				
				ObjectOutputStream os = new ObjectOutputStream(fs);
				
				os.writeObject(gameBar.getBestScore());
								
				os.close();
				
			}catch (Exception ex)
			{
				
			}
		}

		public void	windowDeactivated(WindowEvent e){}

		public void	windowDeiconified(WindowEvent e){}

		public void	windowIconified(WindowEvent e){}

		public void	windowOpened(WindowEvent e){}

	}
}