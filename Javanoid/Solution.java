package mytests.Javanoid;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class Solution
{
	static JFrame frame;
	static GamePanel gamePanel;
	static ThreadKeyLis controller;
	static ThreadMovingBall ballFly;
	static GameBar gameBar;
	static BricksMap bricksMap;
	static Thread gameFPS;
	Dimension barSize;
	Dimension gameSize;
	Dimension frameSize;
	
	public static void main(String [] args)
	{
		bricksMap = new BricksMap();
		bricksMap.ini();
		new Solution().go();
	}
	
	public void go()
	{
		barSize = new Dimension(640, 40);
		gameSize = new Dimension(640, 660); 
		frameSize = new Dimension(640, 680);
				
		frame = new JFrame();
		
		gameBar = new GameBar(bricksMap, this);
		gamePanel = new GamePanel(bricksMap.getMap(), gameBar, bricksMap);
		gameFPS = new Thread(gamePanel);
		gameFPS.start(); 

		Cursor c1 = Toolkit.getDefaultToolkit().createCustomCursor((new ImageIcon(new byte[0])).getImage(), new Point(0,0),	"custom");
		frame.setCursor(c1);

		gameBar.setPreferredSize(barSize);
		gamePanel.setPreferredSize(gameSize);
		frame.setPreferredSize(frameSize);
		
		ballFly = new ThreadMovingBall(frame, gamePanel, gameBar, bricksMap.getMap(), bricksMap);
		controller = new ThreadKeyLis(frame, gamePanel, ballFly, gameBar, bricksMap);

		controller.start();
		ballFly.start();
				
		frame.addKeyListener(controller.new KeyLis());
		frame.addMouseMotionListener(controller.new MouseLis());
		frame.addMouseListener(controller.new MouseKeyLis());
		frame.addWindowListener(controller.new FrameLis());
				
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(BorderLayout.NORTH, gameBar);
		frame.getContentPane().add(BorderLayout.CENTER, gamePanel);
		
		frame.setSize(656, 680);
		Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
		int x1 = (int) ((screenSize.getWidth() - frame.getWidth()) / 2);
		if (x1 < 0) 
		{
			x1 = 0;
		}
		
		int y1 = (int) ((screenSize.getHeight() - frame.getHeight()) / 2);
		if (y1 < 0) 
		{
			y1 = 0;
		}
		
		frame.setVisible(true);
		frame.setBounds(x1, y1, frame.getWidth(), frame.getHeight());
		
	}
	
	public ThreadMovingBall getBall()
	{
		return ballFly;
	}
	
	
}
