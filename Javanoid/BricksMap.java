package mytests.Javanoid;

import mytests.Javanoid.Brick;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class BricksMap
{
	Brick[][] bricks;
	private int xB = 0;
	private int yB = 0;
	private int yTotal = 0;
	
	public void ini()
	{
				
		bricks = new Brick[7][16];
		
		for(int y = 0; y < 7; y++)
		{
			for(int x = 0; x < 16; x++)
			{
				bricks[y][x] = new Brick(xB, yB, 7 - y);
				xB += 40;
			}
			xB = 0;
			yB += 20;
		
		}
		yB = 0;
		
	}
	
	public Brick[][] getMap()
	{
		return bricks;
	}
	
	public void doStep()
	{
		yB += 20;
		yTotal = yB;
		xB = 0;
		for(int y = 0; y < 7; y++)
		{
			for(int x = 0; x < 16; x++)
			{
				bricks[y][x].setXYBrick(xB, yB);
				xB += 40; 
			}
			xB = 0;
			yB += 20;
			
		}
		
		yB = yTotal;
		
	}
	
	public int getYTotal()
	{
		return yTotal;
	}
	
	public void setDefaultMap()
	{
		yTotal = 0;
		yB = 0;
		ini();
	}
}
