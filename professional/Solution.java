package mytests.professional;

/* Считать с консоли URl ссылку.
   Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
   URL содержит минимум 1 параметр.
   Если присутствует параметр obj, то передать его значение в нужный метод alert.
   alert(double value) - для чисел (дробные числа разделяются точкой)
   alert(String value) - для строк

   Пример 1
   Ввод:
   http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
   Вывод:
   lvl view name

   Пример 2
   Ввод:
   http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
   Вывод:
   obj name
   double 3.14
*/

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String obj = "";
        String s  = sc.nextLine();
        String [] str = s.split("\\?");
        String s1 = str[1];
        String  [] str1 = s1.split("=");
        obj+=str1[0] ;
        int  q = 0;
        int z = 0;
        char [] a = s1.toCharArray();
        for (int i = 0; i < a.length; i++)
        {
            if(a[i]=='='){
                q = i;
            }
            if ( a[i]=='&'){
                z = i;
                break;
            }
        }
        String s3 = s1.substring(q+1,z);
        char [] c = s3.toCharArray();
        if(c[0]=='-' && s3.contains(".") || Character.isDigit(c[0]) && s3.contains(".")){

            alert(  Double.parseDouble(s3));

        }
        String [] str2 = s.split("&");
        for (int i = 1; i < str2.length-1; i++)
        {
            obj+= " " + str2[i];
        }
        String s4 = "";
        for (int i = 0; i < str2.length; i++)
        {
            s4 = str2[i];
        }
        String[] s5 = s4.split("=");
        obj+=" " + s5[0];
        alert(obj);


        //add your code here
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}