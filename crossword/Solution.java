package mytests.crossword;

import java.util.Arrays;

/**
 * Created by mops on 16.04.16.
 */
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        String word = "same";
        readWord(crossword, word);
    }

    private static void readWord(int[][] crossword, String word) {
        int currentLetter = word.charAt(0);
        int[] startIndex = {0, 0};
        int[] endIndex = {-1, -1};
        for (int i = 0; i < crossword.length; i++) {
            for (int j = 0; j < crossword[i].length; j++) {
                if (crossword[i][j] == currentLetter) {
                    startIndex[0] = i;
                    startIndex[1] = j;
                    System.out.println("found " +"\'" + (char) currentLetter + "\'" + " on position " +Arrays.toString(startIndex));
                    //найдена первая буква. Теперь надо проверить следующюу букву во всех направлениях
                    //от текущей координаты
                    endIndex = findEndIndex(crossword, startIndex, word);
                }
            }
        }
    }

    private static int[] findEndIndex(int[][] crossword, int[] startIndex, String word) {
         word = word.substring(1);
        return new int[0];
    }
}
