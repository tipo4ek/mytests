package mytests.car;

import java.util.ArrayList;

/**
 * Created by PavelOsipenko on 13.05.2016.
 */
public class Car {
    int height = 160;
    ArrayList<Door> doors = new ArrayList<Door>();

    public Car(){
        doors.add(new Door());
        doors.add(new Door());
        doors.add(new Door());
        doors.add(new Door());
    }
    class Door
    {
        public int getDoorHeight()
        {
            return (int)(height * 0.80);
        }
    }
}
