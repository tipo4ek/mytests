package mytests.InnerClasses;

/**
 * Created by mops on 15.05.16.
 */
public class Tiger extends Cat {
//    private TigerThread thread = new TigerThread();
        private static int countThreads;

    public void tigerRun() {
        System.out.println("Thread " + ++countThreads);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startTiger() {
        TigerThread thread = new TigerThread();
        thread.start();
    }

    class TigerThread extends Thread {

        @Override
        public void run() {
            tigerRun();
        }
    }
}
